// console.log("Happy Monday!");

// Array in programming is simply a list of data. Data that are related/connected with each other.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Now with an array, we can simply write the code above like this;

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

    // [Section] Array
    /*
        -arrays are used to store multiple related values in single variable.
        - they are declared using square brackets([]) also known as "Array literals".
        -Commonly used to store numerous amounts of data to manipulate in order to perform a number of task.
        -array also provide access to a number of function/methods that help achive specific task.
        -Methods is another term for functions associated with an objects/array and is used to execute statements that are relevant.
        -Majority of methos are used to manipulate information stored within the same object.
        -Arrays are also object which is another type.

        Syntax:
            let/const arrayName = [elementA, elementB, elementB . . .]
    */

// Common ex of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac", "Samsung"];
console.log(computerBrands);

// Possible used but not recommended.
let mixedArr = [12, "Asus", null, undefined, {}];
console.log(mixedArr);

// Alternative way to write arrays
let myTask = [
    "drink HTML",
    "eat Javascript",
    "inhale CSS",
    "Baked sass"
    ];

console.log(myTask[1].length);
console.log(myTask);

// Create an array with values from variables.
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];
console.log(cities);

// [Section] length property
// The .length property allows us to get and set the total number of items in an array.

console.log(typeof grades.length);
console.log(cities.length);
// document.write(cities);

let blankArray = [];
console.log(blankArray.length);

let array;
console.log(array);

// lenth property can also be used with strings. Some array methods and properties can be also be used with strings.

let fullName = "John Doe";
console.log(fullName.length);

// length property on strings shows the number of characters in a string. Spaces are counted as character in strings.

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updatong the legnth  property of an array.

let myTasks = [
    "drink HTML",
    "eat Javascript",
    "inhale CSS",
    "Baked sass"
    ];

myTasks.length = myTasks.length-1;
console.log(myTasks);

// to delete specific item in array we can employ array methods (which will be shown in next session) or an algorithm (set of cide to process task.)

// another example using decrementation
cities.length--;
console.log(cities);

// we cant do the same on the same strings
console.log(fullName.length);
fullName.length = fullName.length-1;
console.log(fullName.length);
console.log(fullName);

// If you can shorten the array by setting the length od property, you can also lengthen it by adding a number into the length property.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length + 1;
console.log(theBeatles);

theBeatles[4] = "Roda";
console.log(theBeatles);
// theBeatles[i+1];

// [Section] REading/accession elemenrs of arrays.
    // Accessing array elements is one of the more common task that we do with an array.
    // this can be done through the use of inex.
    // Each element in an array is associated witg its own index/number.

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegend[1]);
console.log(lakersLegend[4]);

// We can alsp save/stpre array elements in another variable.
let currentLaker = lakersLegend[2];

// We can 
console.log("Array before the reassignment:");
console.log(lakersLegend);
lakersLegend[2]= "Pau Gasol";
console.log("Array after the reassignment:")
console.log(lakersLegend);

// Accessing the last elements of an array
// Since the first elements of a array starts at 0, subtractiong 1 to the length of an array will offset the value by one allowing us to get the last element.

let lastElementIndex = lakersLegend.length-1;
console.log(lakersLegend[lastElementIndex]);

// Adding Items into the array, without using the array methods
let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);
newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);


// You can also add at the end of the array. instead of adding it in the front to avaoid the risk of replacinf the firts items ij the arrays.
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

newArr[newArr.length] = "Chris Morter";
// looping over an array/
// you can use a for loop, to iterate over all items n an array.

    for(let index = 0; index <= newArr.length-1; index++){
        console.log(newArr[index]);
    }

// We are going to create a for loop, where it will check whether the elements/numbers are dvisible by 5.
let numArr = [5, 12, 30, 46, 40];

    for(let index = 0; index <= numArr.length-1; index++){
        if(numArr[index]% 5 ===0){
            console.log(numArr[index] + " is divisible by 5.")
        }

        else{
            console.log(numArr[index] + " is not divisible by 5.")
        }
    }

// [Section] Multidimensional Arrays
    // Usefull for dtrong complex data structures.
    // Practical application of this is to help visualize/create real world objects.

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a1', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a1', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a1', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a1', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a1', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
];
console.table(chessBoard);
console.log(chessBoard[4][5]);
console.log(chessBoard[7][3]);